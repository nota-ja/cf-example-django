from django.http import HttpResponse
import sys
import django

def index(request):
    pyver = sys.version.replace("\n", " ")
    djver = django.get_version()
    output = "{\"Python\": \"" + pyver + "\",\n\"Django\": \"" + djver + "\"}"
    return HttpResponse(output)
